from flask import Flask
from flask import render_template
from flask import jsonify
from redis import Redis
import json


app = Flask(__name__)
redis = Redis(host='redis', port=6379)

@app.route('/')
def helloCounter():
   counter = redis.incr('hits')
   return render_template('counter.html', count = counter)

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
