from flask import Flask, render_template
import random

app = Flask(__name__)

# list of cat images
images = [
    "https://i.imgur.com/morIjHE.jpg",
    "https://i.imgur.com/X7E4USR.jpg",
    "https://i.imgur.com/75oT0nj.jpg",
    "https://i.imgur.com/B3epC7y.jpg",
    "https://i.imgur.com/WbrrONl.jpg",
    "https://i.imgur.com/Y0U6PIR.jpg",
    "https://i.imgur.com/Fw3wvxw.jpg",
    "https://i.imgur.com/YYEbT2R.jpg",
    "https://i.imgur.com/JgoYQ7r.jpg",
    "https://i.imgur.com/6PbtrDa.jpg",
    "https://i.imgur.com/TV5WWsb.jpg",
    "https://i.imgur.com/sxN7x3C.jpg",
    "https://i.imgur.com/7qsnmSW.jpg",
    "https://i.imgur.com/jPKDQ7V.jpg",
    "https://i.imgur.com/TbSyXAg.jpg",
    "https://i.imgur.com/cQsvXbe.jpg",
    "https://i.imgur.com/k6H3kV4.jpg",
    "https://i.imgur.com/SpVTwMi.jpg",
    "https://i.imgur.com/uhmGbmg.jpg"
]

@app.route('/')
def index():
    url = random.choice(images)
    return render_template('index.html', url=url)

if __name__ == "__main__":
    app.run(host="0.0.0.0")
